# freepbx-wav-mp3


This feature tested on [FreePBX 15 Distro](https://downloads.freepbxdistro.org/ISO/SNG7-FPBX-64bit-2008-1.iso) based on Asterisk 16.13.0.

### install dependencies
```bash
yum check-update
yum install -y sox lame curl
```

### add script
```bash
su -c "curl https://gitlab.com/check1st/freepbx-wav-mp3/-/raw/master/etc/asterisk/scripts/mixmon_mp3.sh -o /etc/asterisk/scripts/mixmon_mp3.sh --create-dirs" asterisk
chmod +x /etc/asterisk/scripts/mixmon_mp3.sh
```

### update freepbx config
```bash
mysql asterisk --execute 'UPDATE `freepbx_settings` set `value` = "sh /etc/asterisk/scripts/mixmon_mp3.sh ^{YEAR} ^{MONTH} ^{DAY} ^{CALLFILENAME} ^{MIXMON_FORMAT}" where `keyword` = "MIXMON_POST";'
fwconsole r
```

### convert all previous recordings
```bash
su -c "sh /etc/asterisk/scripts/mixmon_mp3.sh * * * * WAV" asterisk
```

#### et voilà!
