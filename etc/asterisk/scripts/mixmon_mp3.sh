#!/bin/sh

for i in `find /var/spool/asterisk/monitor/$1/$2/$3/ -type f -name "$4.$5"`
do
 if [ -e "$i" ]; then
    file=`basename "$i"`;
    filename="${file%.*}";
    dir=`dirname "$i"`;
    sox -t wav "$i" -t wav -e signed-integer - | lame -b 16 -m mono - "$dir/$filename.mp3" && \
    rm -f "$dir/$filename.$5" && \
    mysql asteriskcdrdb --execute='UPDATE cdr SET recordingfile="'$filename'.mp3" WHERE recordingfile="'$file'";';
 fi
done
